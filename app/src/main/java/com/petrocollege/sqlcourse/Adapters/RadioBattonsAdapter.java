package com.petrocollege.sqlcourse.Adapters;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import com.petrocollege.sqlcourse.R;
import com.petrocollege.sqlcourse.Wrappers.RadioButtonWrapp;

import java.util.List;

public class RadioBattonsAdapter extends RecyclerView.Adapter<RadioBattonsAdapter.ViewHolder>
{
    private int mSelectedRadio = -1;
    private Context mContext = null;
    private List<RadioButtonWrapp> mRadioWrapp = null;

    public RadioBattonsAdapter(Context context, List<RadioButtonWrapp> array)
    {
        this.mContext = context;
        this.mRadioWrapp = array;
    }

    public int GetRadioId()
    {
        return mRadioWrapp.get(mSelectedRadio).getId();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = View.inflate(new ContextThemeWrapper(mContext, R.style.MaterialMainThemeNoBar), R.layout.rb_bytton_layout, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        final RadioButtonWrapp wrapp = mRadioWrapp.get(position);

        holder.mButton.setChecked(position == mSelectedRadio);
        holder.mButton.setText(wrapp.getTitle());
    }

    @Override
    public int getItemCount()
    {
        return mRadioWrapp.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        RadioButton mButton = null;
        public ViewHolder(final View itemView)
        {
            super(itemView);
            mButton = itemView.findViewById(R.id.radio_button_simple);

            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    mSelectedRadio = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });


        }
    }

}
