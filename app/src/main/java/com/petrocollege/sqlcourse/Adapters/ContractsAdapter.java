package com.petrocollege.sqlcourse.Adapters;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.petrocollege.sqlcourse.R;
import com.petrocollege.sqlcourse.Wrappers.ContractsWrapp;

import java.util.List;

public class ContractsAdapter extends  RecyclerView.Adapter<ContractsAdapter.ViewHolder>
{
    private Context mContext = null;
    private List<ContractsWrapp> mList = null;

    public ContractsAdapter(Context context, List<ContractsWrapp> array)
    {
        this.mContext = context;
        this.mList = array;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = View.inflate(new ContextThemeWrapper(mContext, R.style.MaterialMainThemeNoBar), R.layout.contract_layout, null);
        return new ContractsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        ContractsWrapp it = mList.get(position);

        holder.mTitle.setText("Контракт №" + it.getId());
        holder.mDate.setText("Дата создания: " + it.getDate());
        holder.mGuid.setText("GUID: " + it.getGuid());
        holder.mProvider.setText("Поставщик: " + it.getProviderTitle());
        holder.mReceiver.setText("Получатель: " + it.getReceiverTitle());
        holder.mCountry.setText("Страна назначения: " + it.getCountry());
        holder.mTotal.setText("Сумма: " + it.getTotal() + " Руб.");

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView mTitle = null;
        MaterialTextView mGuid = null;
        MaterialTextView mDate = null;
        MaterialTextView mProvider = null;
        MaterialTextView mReceiver = null;
        MaterialTextView mCountry = null;
        MaterialTextView mRawMaterial = null;
        MaterialTextView mTotal = null;

        public ViewHolder(final View itemView)
        {
            super(itemView);

            mTitle = itemView.findViewById(R.id.contract_title);
            mGuid = itemView.findViewById(R.id.contract_guid);
            mDate = itemView.findViewById(R.id.contract_date);
            mProvider = itemView.findViewById(R.id.contract_provider);
            mReceiver = itemView.findViewById(R.id.contract_receiver);
            mCountry = itemView.findViewById(R.id.contract_country);
            mRawMaterial = itemView.findViewById(R.id.contract_raw_material);
            mTotal = itemView.findViewById(R.id.contract_total);
        }

    }
}
