package com.petrocollege.sqlcourse.Wrappers;

import java.util.Date;

public class ContractsWrapp
{
    private int id = 0;
    private String date = null;
    private String guid = null;
    private String providerTitle = null;
    private String receiverTitle = null;
    private String country = null;
    private String rawMaterial = null;
    private float total = 0.0f;

    public ContractsWrapp(int id, String date, String guid, String provider, String receiver, String country, String rawMaterial, float total)
    {
        this.id = id;
        this.date = date;
        this.guid = guid;
        this.providerTitle = provider;
        this.receiverTitle = receiver;
        this.country = country;
        this.rawMaterial = rawMaterial;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getGuid() {
        return guid;
    }

    public String getProviderTitle() {
        return providerTitle;
    }

    public String getReceiverTitle() {
        return receiverTitle;
    }

    public String getCountry() {
        return country;
    }

    public String getRawMaterial() {
        return rawMaterial;
    }

    public float getTotal() {
        return total;
    }
}
