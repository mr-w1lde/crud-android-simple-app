package com.petrocollege.sqlcourse.Wrappers;

public class RadioButtonWrapp
{
    private int mId = 0;
    private String mTitle = null;


    public RadioButtonWrapp(int id, String title)
    {
        this.mId = id;
        this.mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }
}
