package com.petrocollege.sqlcourse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;

public class MainActivity extends AppCompatActivity
{

    MaterialButton mLogInButton = null;
    TextInputEditText mEmailView = null;
    TextInputEditText mPasswordView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mLogInButton = findViewById(R.id.login_in_app_button);

        mEmailView = findViewById(R.id.username_field);
        mPasswordView = findViewById(R.id.password_field);

        mLogInButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String username = mEmailView.getText().toString();
                String password = mPasswordView.getText().toString();

//                if(username.isEmpty() || password.isEmpty())
//                {
//                    CreateAlertDialog("Имя пользователя или пароль не могут быть пустыми!");
//                    return;
//                }
//                else if(!username.equals("root") && !password.equals("root"))
//                {
//                    CreateAlertDialog("Имя пользователя и пароль не верны! Возможно вам стоит хакнуть систему и юзать root:root :)");
//                    return;
//                }

                Intent intent = new Intent("com.petrocollege.DataActivity");
                startActivity(intent);

                finish();
            }
        });
    }

    private void CreateAlertDialog(String msg)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.AppAlertDialog);

        alertDialog.setTitle("Ошибка.");
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("Ок", null);

        alertDialog.create().show();
    }
}
