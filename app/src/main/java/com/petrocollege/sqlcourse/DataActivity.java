package com.petrocollege.sqlcourse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.petrocollege.sqlcourse.Adapters.ContractsAdapter;
import com.petrocollege.sqlcourse.Adapters.RadioBattonsAdapter;
import com.petrocollege.sqlcourse.Wrappers.ContractsWrapp;
import com.petrocollege.sqlcourse.Wrappers.RadioButtonWrapp;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class DataActivity extends AppCompatActivity
{
    MaterialButton mCountriesBytton = null;
    MaterialButton mProvidersBytton = null;
    MaterialButton mReceiversBytton = null;
    MaterialButton mRawMaterialssBytton = null;

    MaterialTextView mTextStarted = null;
    RecyclerView mContractsFeed = null;
    ContractsAdapter mContractsAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_activity);

        mTextStarted = findViewById(R.id.start_text_view);
        mContractsFeed = findViewById(R.id.data_recycler);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://146.66.175.175/api/get_contract.php")
                .build();

        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e)
            {
                AlertDialog.Builder errorDialog = new AlertDialog.Builder(DataActivity.this, R.style.AppAlertDialog);

                errorDialog.setTitle("Ошибка.");
                errorDialog.setMessage(e.getLocalizedMessage());
                errorDialog.setPositiveButton("Ок", null);

                errorDialog.show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException
            {
                DataActivity.this.runOnUiThread(new Runnable()
                {
                    final String answer = response.body().string();
                    @Override
                    public void run()
                    {
                        try
                        {
                            JSONObject rootObj = new JSONObject(answer);
                            JSONArray Jarray = rootObj.getJSONArray("contracts");

                            if(Jarray.length() <= 0)
                            {
                                mTextStarted.setText("Нет доступных контрактов.");
                                return;
                            }

                            mTextStarted.setVisibility(View.GONE);

                            List<ContractsWrapp> arr = new ArrayList<ContractsWrapp>();

                            for(int i=0; i < Jarray.length(); i++)
                            {
                                JSONObject it = Jarray.getJSONObject(i);
                                arr.add(new ContractsWrapp(
                                        Integer.parseInt(it.optString("idContract")),
                                        it.optString("Date"),
                                        it.optString("GUID"),
                                        it.optString("ProviderTitle"),
                                        it.optString("ReceiverTitle"),
                                        it.optString("CountryTitle"),
                                        it.optString("MaterialTitle"),
                                        Float.parseFloat(it.optString("TotalCount"))

                                ));
                            }


                            mContractsAdapter = new ContractsAdapter(DataActivity.this, arr);

                            mContractsFeed.setHasFixedSize(true);
                            mContractsFeed.setLayoutManager(new LinearLayoutManager(DataActivity.this));

                            mContractsFeed.setAdapter(mContractsAdapter);
                            mContractsFeed.setItemAnimator(new DefaultItemAnimator());

                        }
                        catch (Exception e)
                        {

                        }
                    }
                });
            }
        });

    }

    private void OpenApiDialog(final String url, final String arrayTitle, final String idTitle, final String dialogTitle, final DialogInterface.OnClickListener listener)
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e)
            {
                DataActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DataActivity.this, R.style.AppAlertDialog);

                        alertDialog.setTitle("Ошибка.");
                        alertDialog.setMessage("Не удалось получить данные!");
                        alertDialog.setPositiveButton("Ок", null);

                        alertDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException
            {
                DataActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(DataActivity.this, R.style.AppAlertDialog)).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.radio_button_layout, null);

                        final List<RadioButtonWrapp> array = new ArrayList<>();

                        try
                        {
                            JSONArray jsonArray = (new JSONObject(response.body().string())).getJSONArray(arrayTitle);

                            for(int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject it = jsonArray.getJSONObject(i);

                                int id = Integer.parseInt(it.optString(idTitle).toString());
                                String titile = it.optString("Title").toString();

                                array.add(new RadioButtonWrapp(id, titile));
                            }
                        }
                        catch (Exception e)
                        {
                            AlertDialog.Builder errorDialog = new AlertDialog.Builder(DataActivity.this, R.style.AppAlertDialog);

                            errorDialog.setTitle("Ошибка.");
                            errorDialog.setMessage(e.getLocalizedMessage());
                            errorDialog.setPositiveButton("Ок", null);

                            errorDialog.show();
                        }


                        RecyclerView curr = dialogView.findViewById(R.id.recycler_radio);
                        final RadioBattonsAdapter adapter = new RadioBattonsAdapter(DataActivity.this, array);

                        curr.setHasFixedSize(true);
                        curr.setLayoutManager(new LinearLayoutManager(getParent()));

                        curr.setAdapter(adapter);
                        curr.setItemAnimator(new DefaultItemAnimator());

                        alertDialog.setView(dialogView);
                        alertDialog.setTitle(dialogTitle);
                        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Выбрать", listener);
                        alertDialog.show();
                    }
                });
            }
        });
    }




}
